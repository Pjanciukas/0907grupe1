<html>
    <head>
        <title>Drop</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <style type="text/css">
            .buttonbutton{

                margin: 0 auto;
            }
        </style>
    </head>
    <body class="landing"> 
                   

            <section id="banner">
                <div class="content">
                    <header>
                        <h2><?php 


						if(isset($_GET["vardas"])) {
							echo $_GET["vardas"] . ",";
						} else {
							echo "Mielas vartotojau";
						}

						?></h2>
                        <p>Gurmanų bendruomenė sveikina jus sėkmingai prisiregistravus!</p>
                        <a href="login.php" class="buttonbutton button special">Prisijungti</a>
                    </header>
                    <span class="image"><img src="images/recipess.jpg" alt="" /></span>
                </div>
                <a href="#one" class="goto-next scrolly">Next</a>
            </section>

            <!-- One -->

            <footer id="footer">
            <h3>Follow us on:</h3>
                <ul class="icons">
                    <li><a href="#" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
                    <li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
                    <li><a href="#" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
                    <li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
                    <li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
                    <li><a href="#" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
                </ul>
                <ul class="copyright">
                    <li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
                </ul>
            </footer>

        </div>

        <!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.scrolly.min.js"></script>
        <script src="assets/js/jquery.dropotron.min.js"></script>
        <script src="assets/js/jquery.scrollex.min.js"></script>
        <script src="assets/js/skel.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/main.js"></script>

    </body>
</html>