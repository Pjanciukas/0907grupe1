<?php

session_start();

if(!isset($_SESSION['username'])) {
	
	header("Location: index.html");
	
} 
	
$username = $_SESSION['username'];
	
	?>


<html>
    <head>
        <title>Drop</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="assets/css/main.css" />
        <style type="text/css">
            .headinggg{
                text-align: center;
                margin-top: 150px;
                font-size: 30px;
            }
            #headriukas{
                text-align: center;
                width: 50%;
                margin: 0 auto;
            }
            #category{
                width: 50%;
                margin: 0 auto;
            }
            #message{
                width: 70%;
                margin: 0 auto;
            }
            #butonukas{
               width: 20%;
            margin-left: 60%; 
            }
            #hh{
                height: 400px;
                width: 80%;
            }

        </style>
    </head>
    <body class="landing">  
        <div id="page-wrapper">

            <!-- Header -->
            <header id="header">
                <h1 id="logo"><a href="index.html">Pagrindinis</a></h1>
                <nav id="nav">
                    <ul>
                        <li><a href="index.html">Grįžti į receptų peržiūrą</a></li>
                        
                        <li><a href="logout_db.php" class="button special">Atsijungti</a></li>
                    </ul>
                </nav>
            </header>
            
          
            <div>
                <h1 class="headinggg">Naujo recepto įvedimas</h1>
				<h4 style="text-align: center;">Įveskite valgio pavadinimą</h4>
                
                <form action="db_newrecipe.php" method="post">
                <input type="text" name="description" id="headriukas" placeholder="valgio pavadinimas">
                <br>
                <h4 style="text-align: center;">Pasirinkite receptų grupę</h4>
                <select name="recipe_group" id="category" default="none">
                    <option value="Desertai">desertai</option>
                    <option value="Sriubos">sriubos</option>
                    <option value="Koses">košės</option>
                    <option value="Salotos">salotos</option>
                    <option value="Kompotai">kompotai</option>
                    </select>
                    <br>
                    <div class="container" >
                      <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
                      <script>tinymce.init({ selector:'textarea' });</script>
                      <textarea id="hh" name="content" id="message"  placeholder="Please enter the description of your recipie" rows="6"></textarea></div>    
                <br>
                <input type="Submit" id="butonukas" value="Išsaugoti">

                </form>
            </div>
           
            <!-- Footer -->
            <footer id="footer">
                <ul class="icons">
                    <li><a href="#" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
                    <li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
                    <li><a href="#" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
                    <li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
                    <li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
                    <li><a href="#" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
                </ul>
                <ul class="copyright">
                    <li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
                </ul>
            </footer>

        </div>

        <!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.scrolly.min.js"></script>
        <script src="assets/js/jquery.dropotron.min.js"></script>
        <script src="assets/js/jquery.scrollex.min.js"></script>
        <script src="assets/js/skel.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/main.js"></script>

    </body>
</html>