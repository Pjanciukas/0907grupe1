<?php


$error = null;
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $myusername = $_POST['username'];

	   setcookie("rememberme", $myusername); 
		$_SESSION['favcolor'] = 'green';
		$_SESSION['username'] = $myusername;

            header("location: welcome.php");

}
?>

<html>
    <head>
        <title>Prisijungimas</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="assets/css/main.css" />
    </head>
    <body class="landing">   
        <div id="page-wrapper">


            <header id="header">
                <h1 id="logo"><a href="index.html">Atgal</a></h1>
                <nav id="nav">
                   
                </nav>
            </header>


            <section id="four" class="wrapper style1 special fade-up">
                <div class="container">
                    <header class="major">
                        <h2>Prisijungimas</h2>
                        <?php
                        if ($error != null) {
                            ?>
                            <p class="actions">Blogas slaptazodis</p>	
                            <?php
                        }
                        ?>
                        <form action="login_db.php" method="post">
                            <div class="row uniform 50%">
                                <div class="6u 12u$(xsmall)">
                                    <input type="text" name="vardas" placeholder="Vardas" required>
                                </div>
                                <div class="6u$ 12u$(xsmall)">
                                    <input type="password" name="slaptazodis" placeholder="slaptazodis" required>
                                </div>
                                <div class="12u$">
                                    <ul class="actions">
									<li><input type="Submit" value="Prisijungti" class="special" /></li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                    </header>
                </div>
            </section>
			<center><p>Neturite paskyros? <a href="Registracija.php" class="button special small">Registracija</a><p></center>
                    
					
            <script>
<!-- Footer -->
                < footer id = "footer" >
                        < ul class = "icons" >
                        < li > < a href = "#" class = "icon alt fa-twitter" > < span class = "label" > Twitter < /span></a > < /li>
                                    < li > < a href = "#" class = "icon alt fa-facebook" > < span class = "label" > Facebook < /span></a > < /li>
                                                < li > < a href = "#" class = "icon alt fa-linkedin" > < span class = "label" > LinkedIn < /span></a > < /li>
                                                            < li > < a href = "#" class = "icon alt fa-instagram" > < span class = "label" > Instagram < /span></a > < /li>
                        < li > < a href = "#" class = "icon alt fa-github" > < s pan class = " label" > GitHub  < /span></a > < /li>
                                                                                    < li > < a href = "#" class = "icon alt fa-envelope" > < span class = "label" > Email < /span></a > < /li>
                                                                                                < /ul>
                                                                                                < ul class = "copyright" >
                                                                                                < li > & copy; Untitled.All rights reserved. < /li><li>Design: <a href="http:/ / html5up.net">HTML5 UP</a></li>
                                                                                                < /ul>
                                                                                                < /footer>

                                                                                                < /div>

<!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
            <script src="assets/js/jquery.scrolly.min.js"></script>
            <script src="assets/js/jquery.dropotron.min.js"></script>
            <script src="assets/js/jquery.scrollex.min.js"></script>
            <script src="assets/js/skel.min.js"></script>
            <script src="assets/js/util.js"></script>
            <script src="assets/js/main.js"></script>

    </body>
</html>