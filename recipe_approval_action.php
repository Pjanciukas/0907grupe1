<?php

include ("config.php");

$recipe_id = $_POST['recipe_id'];

$selected_radio = $_POST['approval'];

$sql = "SELECT 'approval' FROM recipes WHERE id = '{$recipe_id}'";

$results = $db->query($sql);

if($results->num_rows === 1) {
	
	if($selected_radio == 'approve') {
		
		$sql = "UPDATE recipes SET approval = 'patvirtintas' WHERE id = {$recipe_id}";
		$results = $db->query($sql);
		
	} else if($selected_radio == 'remove_approval') {
		
		$sql = "UPDATE recipes SET approval = 'nepatvirtintas' WHERE id = {$recipe_id}";
		$results = $db->query($sql);
		
	}
		
	header('Location: recipe_approval.php');
	
} else {
	
	echo "Klaida, recepto su id " . $recipe_id . " nera";
	
}

