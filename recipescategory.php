<!DOCTYPE HTML>
<html lang="lt">
    <head>
        <title>Receptai</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="assets/css/main.css" />
		
		<style>
		
		
		
		</style>
	
		
    </head>
    <body class="landing">   
        <div id="page-wrapper">
		
		
			<!-- Header -->
            <header id="header">
                
                <nav id="nav">
                    <ul>
                        <li><a href="index.html">Pagrindinis</a></li>
                        <li>
                            <a href="#">Kategorijos</a>
							<ul>
                                <li><a href="#">Pagrindiniai patiekalai</a></li>
                                <li><a href="#">Salotos</a></li>
                                <li><a href="category.php?category">Sriubos</a></li>
                                <li><a href="#">Užkandžiai</a></li>
                                <li><a href="#">Desertai</a></li>
                                <li><a href="#">Gėrimai</a></li>
                            </ul>
                        </li>
                        <li><a href="enternewrecipie.php" class="button special">Įkelti receptą</a></li>
                    </ul>
                </nav>
            </header>

				<section id="banner">
					<div class="content">
						<h2><center>Pasirinkite, ko ieškosite:</center></h2>
							<div>
							<a href="#" class="button fit">Pagrindiniai patiekalai</a>
							<a href="#" class="button fit">Salotos</a>
							<a href="#" class="button fit">Sriubos</a>
							</div>
							<div>
							<a href="#" class="button fit">Užkandžiai</a>
							<a href="#" class="button fit">Desertai</a>
							<a href="#" class="button fit">Gėrimai</a>
							</div>
						
				
					</div>
				</section>
				
				<!-- Footer -->
					<footer id="footer">
						<ul class="icons">
							<li><a href="#" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
							<li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
							<li><a href="#" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
							<li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
							<li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
							<li><a href="#" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
						</ul>
						<ul class="copyright">
							<li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
						</ul>
					</footer>
       
        </div>
		 <!-- Scripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/jquery.scrolly.min.js"></script>
        <script src="assets/js/jquery.dropotron.min.js"></script>
        <script src="assets/js/skel.min.js"></script>
        <script src="assets/js/util.js"></script>
        <script src="assets/js/main.js"></script>

    </body>
</html>