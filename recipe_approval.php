﻿<?php

session_start();

if(!isset($_SESSION['username'])) {
	
	header("Location: index.html");
	
} else if($_SESSION['rights'] != "administratorius") {
	
	header("Location: index.html");
	
}
	
	?>


<html>
	<head>
		<title>Receptų patvirtinimas</title>
		<meta charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body>
		<div id="page-wrapper">

			<!-- Header -->
				<header id="header">
					<h1 id="logo"><a href="index.html">Pagrindinis</a></h1>
					<nav id="nav">
						<ul>
							<li><a href="index.html"></a></li>
							
							
							<li><a href="logout_db.php" class="button special">Atsijungti</a></li>
						</ul>
					</nav>
				</header>

			<!-- Main -->
				<div id="main" class="wrapper style1">
					<div class="container">
						<header class="major">
							<h3>Receptų patvirtinimas</h3>
							<p>Prašome peržiūrėti receptus ir patvirtinti juos arba atšaukti patvirtinimą ir susiekti su vartotoju dėl receptų koregavimo</p>
							
						</header>


					<!-- Form -->
                    <section>
                        
						
						
						
						
						
                        <form method="post" action="recipe_approval_filter.php">
                            <div class="row uniform 50%">
                                
                                <div class="12u$">
                                    <div class="select-wrapper">
                                        <select name="category" id="category">
                                            <option value="visi_receptai">
											<?php
												if(isset($_GET["receiptCategory"])) {
													$receiptCategoryDescription = $_GET["receiptCategory"];
													if($receiptCategoryDescription == "visi_receptai") {
														$receiptCategoryDescription = "- Visos kategorijos -";
													}
												} else {
													$receiptCategoryDescription = "- Visos kategorijos -";
												}
												echo $receiptCategoryDescription;
											?></option>
											
											<?php
												
												if($receiptCategoryDescription != "- Visos kategorijos -") {
													echo "<option value='visi_receptai'>- Visos kategorijos -</option>";
												}
												
												include ("config.php");
												
												$sql = "SELECT * FROM recipe_groups";
												
												$results = $db->query($sql);
												
												if($results->num_rows > 0) 
												{
													while($row = $results->fetch_assoc()) 
													{
														$group_id = $row['id'];
														$group_name = $row['group_name'];
														echo "<option value='" . $group_name . "'>" . $group_name . "</option>";
														
													}
												} else {
													echo "0 irasu";
												}
											
											?>											
                                            
                                        </select>
                                    </div>
                                </div>
                               
                                <div class="12u$">
                                    <ul class="actions">
                                        <li><input type="submit" value="Filtruoti" class="special" /></li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </form>
                    </section>

						

							
							
							  <!-- Table -->
                    <section>
                        
                        
                        <div class="table-wrapper">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Pavadinimas</th>
										<th>Aprašymas</th>
										<th>Kategorija</th>
                                        <th>Būsena</th>
										<th>Veiksmai</th>
                                        <th>id</th>
										
                                    </tr>
                                </thead>
                                <tbody>
                                    
									
									<?php
								
									if(isset($_GET["receiptCategory"])) {
										$receiptCategory = $_GET["receiptCategory"];
									} else {
										$receiptCategory = "visi_receptai";
									}
									
									if($receiptCategory == "visi_receptai") {
										$sql = "SELECT * FROM recipes";
									} else {
										$sql = "SELECT * FROM recipes WHERE recipe_group = '{$receiptCategory}'";
									}								
									

									$results = $db->query($sql);
								
								if($results->num_rows > 0) 
								{
									while($row = $results->fetch_assoc()) 
									{
										?>
										<tr>
											<td><?php echo $row['description']; ?></td>
											<td><?php echo $row['content']; ?></td>
											<td><?php echo $row['recipe_group']; ?></td>
											<td>
												<?php
													if($row['approval'] == 'patvirtintas') {
														echo "<b><font color='green'>" . $row['approval'] . "</font></b>";
														$new_status = "remove_approval";
														$new_status_text = "Atšaukti patvirtinimą";
													} else {
														echo "<font color='red'>" . $row['approval'] . "</font>";
														$new_status = "approve";
														$new_status_text = "Patvirtinti";
													}
												?>
											</td>
											<td>
											<?php
												echo "<a href='recipe_approval_with_link.php?recipe_id=" . $row['id'] . "&new_status=" . $new_status . "'>" . $new_status_text . "</a>";
																						
											?>											
											</td>
											<td><b> <?php echo $row['id']; ?></b></td>
																					
										</tr>
										<?php		
									}
								} else {
									echo "0 irasu";
								}
									?>
										
                                    
                                </tbody>
                               
                            </table>
                        </div>
                        
                    </section>
							
						
							                    <!-- Form -->
                    <section>
                        <h4>Įveskite recepto id</h4>
                        <form method="post" action="recipe_approval_action.php">
                            <div class="row uniform 50%">
                                <div class="6u 12u$(xsmall)">
                                    <input type="text" name="recipe_id" id="name" value="" placeholder="recepto id" required>
                                </div>
								<br>
								<br>
								<br>
                                <div class="4u 12u$(medium)">
                                    <input type="radio" id="approve" name="approval" value="approve" checked="checked">
                                    <label for="approve">Patvirtinti</label>
                                </div>
                                <div class="4u 12u$(medium)">
                                    <input type="radio" id="remove_approval" name="approval" value="remove_approval">
                                    <label for="remove_approval">Atšaukti patvirtinimą</label>
                                </div>
                               
                                <div class="12u$">
                                    <ul class="actions">
                                        <li><input type="submit" value="Vykdyti" class="special" /></li>
                                        <li><input type="reset" value="Išvalyti" /></li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                    </section>
							
							
							
							
					</div>
				</div>
				
				
				
				

			<!-- Footer -->
				<footer id="footer">
					<ul class="icons">
						<li><a href="#" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="#" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="#" class="icon alt fa-linkedin"><span class="label">LinkedIn</span></a></li>
						<li><a href="#" class="icon alt fa-instagram"><span class="label">Instagram</span></a></li>
						<li><a href="#" class="icon alt fa-github"><span class="label">GitHub</span></a></li>
						<li><a href="#" class="icon alt fa-envelope"><span class="label">Email</span></a></li>
					</ul>
					<ul class="copyright">
						<li>&copy; Vilnius Coding School. All rights reserved.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
					</ul>
				</footer>

		</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/jquery.dropotron.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>